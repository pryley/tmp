<?php

namespace Statamic\Addons\VanityUrl;

use Statamic\API\Config;
use Statamic\Extend\Tags;

/**
 * Addon for Statamic 2
 */

class VanityUrlTags extends Tags
{
	/**
	 * Gets the vanity url of an url
	 *
	 * Usage: {{ vanity_url }}
	 *
	 * @return string
	 */
	public function index()
	{
		$url = $this->getUrl();

		$vanity_urls = Config::get( 'routes.vanity' );

		if( array_key_exists( $url, $vanity_urls ) ) {

			$vanity_url = $vanity_urls[ $url ];

			if( substr( $vanity_url, 0, 1 ) != '/' ) {
				$vanity_url = '/' . $vanity_url;
			}

			$url = $vanity_url;
		}

		return $url;
	}

	/**
	 * Checks if the current url has a vanity url
	 *
	 * Usage: {{ if { vanity_url:check } }}...{{ /if }}
	 *
	 * @return bool
	 */
	public function check()
	{
		return $this->getUrl() !== $this->index();
	}

	protected function getUrl()
	{
		$url = $this->get( 'url' );

		if( !$url && isset( $this->context['url'] ) ) {
			$url = $this->context['url'];
		}

		return $url;
	}
}
