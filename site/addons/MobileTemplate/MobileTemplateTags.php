<?php

namespace Statamic\Addons\MobileTemplate;

use Statamic\API\Arr;
use Statamic\API\Config;
use Statamic\API\File;
use Statamic\API\Parse;
use Statamic\Data\DataCollection;
use Statamic\Extend\Tags;

/**
 * Addon for Statamic 2
 */

class MobileTemplateTags extends Tags
{
	public function __call( $method, $arguments )
	{
		$template = $this->loadTemplate( $this->get( 'src' ) );

		// Allow parameters to be variable names and retrieve them from
		// context. If they don't exist, they fall back to the string.
		$variables = [];
		foreach( $this->parameters as $key => $param ) {
			$variables[ $key ] = array_get( $this->context, $param, $param );
		}

		$variables = array_merge( $variables, $this->context );

		// var_dump( print_r( array_keys( $variables ), 1 ) );

		return Parse::template( $template, $variables );
	}

	/**
	 * Gets the raw contents of the template
	 *
	 * @return string
	 */
	protected function loadTemplate( $template )
	{
		$template_path = "templates/mobile/{$template}.html";

		if( File::disk( 'theme' )->exists( $template_path ) ) {
			return File::disk( 'theme' )->get( $template_path );
		}

		$template_path = "templates/mobile/default.html";

		if( File::disk( 'theme' )->exists( $template_path ) ) {
			return File::disk( 'theme' )->get( $template_path );
		}

		$template = Config::get( 'theming.default_page_template' );
		$template_path = "templates/mobile/{$template}.html";

		if( File::disk( 'theme' )->exists( $template_path ) ) {
			return File::disk( 'theme' )->get( $template_path );
		}
	}
}
