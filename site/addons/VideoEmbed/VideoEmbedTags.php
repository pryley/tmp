<?php

namespace Statamic\Addons\VideoEmbed;

use Statamic\API\Asset;
use Statamic\Extend\Tags;

/**
 * Addon for Statamic 2
 */

class VideoEmbedTags extends Tags
{
	/**
	 * {{ video_embed url="" }}
	 */
	public function index()
	{
		return $this->generateVideoEmbedUrl( $this->get( 'url' ) );
	}

	/**
	 * {{ video_embed:plyr url="" }}
	 */
	public function plyr()
	{
		$url = $this->generateVideoEmbedUrl( $this->get( 'url' ) );

		if( $embed = $this->extractVideoIdFrom( $url ) ) {
			return sprintf( '<div class="js-plyr" data-type="%s" data-video-id="%s"></div>',
				key( $embed ),
				$embed[ key( $embed ) ]
			);
		}
	}

	protected function generateVideoEmbedUrl( $url )
	{
		// https://i.ytimg.com/vi/{$id}/maxresdefault.jpg

		$services = array(
			'youtube' => "https://www.youtube.com/embed/%s?showinfo=0&modestbranding=1",
			'vimeo'   => "https://player.vimeo.com/video/%s?title=0&byline=0&portrait=0&badge=0&color=ffffff",
		);

		if( $embed = $this->extractVideoIdFrom( $url )) {
			$url = sprintf( $services[ key( $embed ) ], $embed[ key( $embed ) ]);
			return sprintf( '<iframe src="%s" frameborder="0" webkitAllowFullScreen mozallowfullscreen allowFullScreen></iframe>', $url );
		}
	}

	protected function extractVideoIdFrom( $url )
	{
		if( preg_match( '/youtube\.com\/watch\?v=([^\&\?\/]+)/', $url, $id )
			|| preg_match( '/youtube\.com\/embed\/([^\&\?\/]+)/', $url, $id )
			|| preg_match( '/youtube\.com\/v\/([^\&\?\/]+)/', $url, $id )
			|| preg_match( '/youtu\.be\/([^\&\?\/]+)/', $url, $id ) ) {
			return array( 'youtube' => $id[1] );
		}
		else if( preg_match( '/vimeo\.com\/(\d+$)/', $url, $id ) ) {
			return array( 'vimeo' => $id[1] );
		}

		return false;
	}
}
