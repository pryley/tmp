<?php

namespace Statamic\Addons\Uid;

use Statamic\Extend\Tags;

/**
 * Addon for Statamic 2
 */

class UidTags extends Tags
{
	/**
	 * Generates a unique ID
	 *
	 * Usage: {{ uid }}
	 *
	 * @return string
	 */
	public function index()
	{
		return md5( mt_rand() );
	}
}
